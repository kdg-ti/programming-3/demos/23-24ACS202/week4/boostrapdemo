package be.kdg.programming3.bootstrapdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootstrapdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootstrapdemoApplication.class, args);
    }

}
